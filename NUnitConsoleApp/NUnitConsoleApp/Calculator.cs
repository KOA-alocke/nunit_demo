﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitConsoleApp
{
    public class Calculator
    {

        public int Add(int digitOne, int digitTwo)
        {
            return digitOne + digitTwo;
        }

        public int Sub(int digitOne, int digitTwo)
        {
            return digitOne - digitTwo;
        }

        public int Multiply(int digitOne, int digitTwo)
        {
            return digitOne * digitTwo;
        }

        public double Dvision(int digitOne, int digitTwo)
        {
            return digitOne % digitTwo;
        }

    }
}
