﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitConsoleApp
{
    class UserInput
    {
        public UserInput()
        {
            
        }

        public string[] GetUserInputsAsList()
        {
            string[] inputs = new string[2];
            Console.Write("Add first input: ");
            inputs[0] = Console.ReadLine();
            Console.Write("Add second input: ");
            inputs[1] = Console.ReadLine();
            return inputs;
        }
    }
}
