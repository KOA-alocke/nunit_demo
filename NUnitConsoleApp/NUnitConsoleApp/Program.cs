﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NUnitConsoleApp
{
    class Program
    {

        private static bool verifyPass;
        private static int digitOne;
        private static int digitTwo;


        static void Main(string[] args) //LOOK AT THAT MAIN FUNCTION
        {
            GetAndVerifyData(args);  //THAT IS ONE SERIOUS MAIN FUNCTION
            CalculateAndPrint();          
        } //SHORTEST MAIN FUNCTION YOU WILL EVER SEE


        private static void GetAndVerifyData(string[] args) //this function is awesome
        {
            if (args.Length == 2)
            {
                ConvertParameters(args);
            }
            else
            {
                UserInput userInput = new UserInput();
                string[] inputs = userInput.GetUserInputsAsList();
                ConvertParameters(inputs);
            }
        }


        private static void CalculateAndPrint() //this function is also awesome
        {
            if (verifyPass)
            {
                var calculator = new Calculator();
                Console.WriteLine(calculator.Multiply(digitOne, digitTwo));
                Console.ReadKey();
            }
        }


        private static void ConvertParameters(string[] inputStrings)// this function is among those that are awesome
        {
            var digitOneCheck = int.TryParse(inputStrings[0], out digitOne);
            var digitTwoCheck = int.TryParse(inputStrings[1], out digitTwo);
            CheckPass(digitOneCheck,digitTwoCheck);
        }


        private static void CheckPass(bool digitOneCheck, bool digitTwoCheck)//this awesome thing is, in fact, a function
        {
            if (digitOneCheck && digitTwoCheck)
            {
                verifyPass = true;
            }
            else
            {
                verifyPass = false;
            }
        }
    }
}
