﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;



namespace NUnitConsoleApp.Tests
{
    [TestFixture]
    public class CalculatorTest
    {
        [Test]
        public void MultiplyIsCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(27, result);
        }

        [Test]
        public void MultiplyIsNotCorrect()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreNotEqual(81, result);
        }

        [Ignore]
        [Test]
        public void MultiplyFails()
        {
            var calc = new Calculator();
            var result = calc.Multiply(9, 3);

            Assert.AreEqual(81,result);
        }

        [TestCase(12,3,Result = 36)]
        public int MultiplyGoodNumbers(int d1, int d2)
        {
            var calc = new Calculator();
            return calc.Multiply(d1, d2);
        }

    }
}
